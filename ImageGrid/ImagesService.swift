//
//  ImagesService.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/7/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import Foundation

final class ImagesService: BaseService {

    /**
    Cached images objects.
    */
    var images: [Image?] = []
    /**
    Cached images identifiers.
    */
    var imageIds: [String]?

    /**
    Fetch images.
    - parameter completion:  A closure that takes an optional `NSError` object.
    */
    func fetchImages(completion: ErrorCompletion) {
        // Clear image cache
        reset()
        fetchImageIds { (error) -> Void in
            guard let error = error,
                _ = self.imageIds else {

                    let group = dispatch_group_create()
                    for imageId in self.imageIds! {
                        dispatch_group_enter(group)
                        self.fetchSingleImage(imageId, completion: { (error) -> Void in
                            guard let error = error else {
                                dispatch_group_leave(group)
                                return
                            }
                            completion(error: error)
                        })
                    }
                    dispatch_group_notify(group, dispatch_get_main_queue(), { () -> Void in
                        completion(error: nil)
                    })
                    return
            }

            completion(error: error)
        }
    }

}

private extension ImagesService {

     /**
    Fetch a single image given an id.

    - parameter id:   the id of the image to fetch
    - parameter completion: a closure returning an optional ErrorType object
    */
    func fetchSingleImage(id: String, completion: ErrorCompletion) -> Void {
        let request = client.fetchSingleImage(id) { (result) in
            switch result {
            case .Success(let JSON):
                for (key, value) in JSON {
                    if key == "url" {
                        if let url = value as? String {
                            let image = Image(imageUrl: url)
                            self.images.append(image)
                        }
                    }
                }
                completion(error: nil)
                break
            case .Failure(let error):
                completion(error: error)
                break
            }
        }

        notePendingRequest(request)
    }

    /**
    Fetch image ids.
    - parameter completion:  A closure that takes an optional `NSError` object.
    */
    func fetchImageIds(completion: ErrorCompletion) -> Void {
        let request = client.fetchImagesIds { (result) in
            switch result {
            case .Success(let JSON):
                for (key, value) in JSON {
                    if key == "image_ids" {
                        self.imageIds = value as? [String]
                    }
                }
                completion(error: nil)
                break
            case .Failure(let error):
                completion(error: error)
                break
            }
        }

        notePendingRequest(request)
    }

    /**
    Clear image cache.
    */
    func reset() -> Void {
        images = []
    }
}
