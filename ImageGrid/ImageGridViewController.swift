//
//  ViewController.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/7/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import UIKit

final class ImageGridViewController: UIViewController {

    var viewModel: ImageGridViewModel?
    var gridView: ImageGridView = ImageGridView(frame: CGRectZero)
    let gestureManager: GestureManager = GestureManager()
    private var collectionView: UICollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: UICollectionViewFlowLayout())
    private let onboardingViewController = OnboardingViewController()

    convenience init(viewModel: ImageGridViewModel) {
        self.init()
        self.viewModel = viewModel
        self.viewModel?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()
        configureView()
        configureLayout()
        configureOnboardingViewController()
    }

}

private extension ImageGridViewController {

    /**
    Set colors, subviews for view.
    */
    func configureView() -> Void {
        gridView.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = snazzyBluishColor

        view.addSubview(gridView)
        collectionView.dataSource = self
        collectionView.delegate = self
        gridView.collectionView = collectionView
        
    }

    /**
    Set delegate, gestureRecognizer for collectionView.
    */
    func configureCollectionView() -> Void {
        collectionView.registerClass(ImageGridCollectionViewCell.self, forCellWithReuseIdentifier: "com.cellReuseIdentifier")
        let layout = UICollectionViewFlowLayout()
        collectionView.collectionViewLayout = layout
        gestureManager.view = collectionView
        gestureManager.delegate = self
        collectionView.addGestureRecognizer(gestureManager.gestureRecognizer)
    }

    /**
    Check for empty image cache, set delegae on onboarding VC.
    */
    func configureOnboardingViewController() -> Void {
        onboardingViewController.delegate = self

        // Load cached images
        if let viewModel = viewModel {
            viewModel.load()
            if viewModel.collectionList.isEmpty {
                animateOnboardingViewController(true)
            }
        }
    }
    
    /**
    Add layout constraints to view/subviews
    */
    func configureLayout() -> Void {

        let topConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: gridView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: -25.0)
        let leadingConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: gridView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: -5.0)
        let trailingConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: gridView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 5.0)
        let bottomConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: gridView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 5.0)

        view.addConstraints([
                topConstraint,
                leadingConstraint,
                trailingConstraint,
                bottomConstraint
            ])
    }

    /**
    Animate onboardingViewController alpha
    */
    func animateOnboardingViewController(visible: Bool) -> Void {
        onboardingViewController.view.alpha = visible ? 0.0 : 1.0

        if visible {
            addChildViewController(onboardingViewController)
            view.addSubview(onboardingViewController.view)
        }

        UIView.animateWithDuration(1.0, animations: { () -> Void in
            self.onboardingViewController.view.alpha = visible ? 1.0 : 0.0
            }) { (Bool) -> Void in
                if (!visible) {
                    self.onboardingViewController.removeFromParentViewController()
                    self.onboardingViewController.view.removeFromSuperview()
                }
        }
    }

}

extension ImageGridViewController: ImageGridViewModelDelegate {

    /**
    Handle image fetch failure, display alert dialog to user on error.
    */
    func imageFetchDidFail(errorMessage: String) {
        let errorMessage = NSLocalizedString(errorMessage, comment: "Error message to be displayed to the user")
        let errorTitle = NSLocalizedString("Oops", comment: "Title for error dialog")
        let alert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Close", comment: "dismissal button title"), style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            // Cue onboarding sequence in event of error
            self.onboardingViewController.displayMode = DisplayMode.Onboarding
            self.animateOnboardingViewController(true)
        }))
        presentViewController(alert, animated: true, completion: nil)
    }

    /**
    Reload data when images are fetched from the server.
    */
    func imageGridViewModelDidFetchImages() {
        collectionView.reloadData()
    }

    /**
     Cue "refesh images" overlay.
    */
    func imageGridViewModelImagesCacheDidEmpty() {
        onboardingViewController.displayMode = .Refresh
        animateOnboardingViewController(true)
    }
    
}

extension ImageGridViewController: UICollectionViewDataSource {

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let image: Image? = viewModel?.collectionList[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("com.cellReuseIdentifier", forIndexPath: indexPath) as! ImageGridCollectionViewCell
        cell.imageUrl = image?.imageUrl
        return cell
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.collectionList.count ?? 0
    }

}

extension ImageGridViewController: UICollectionViewDelegate {

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(85, 85)
    }

}

extension ImageGridViewController: OnboardingViewControllerDelegate {

    /**
    Fetch images from S3.
    */
    func fetchImagesButtonTapped() {
        viewModel?.fetchImages({ () -> () in
            self.animateOnboardingViewController(false)
        })
    }

}

extension ImageGridViewController: GestureManagerDelegate {

    /**
    - returns: an ImageGridCollectionViewCell at the specified touch location.
    */
    func cellForTouchLocation(touchLocation: CGPoint) -> ImageGridCollectionViewCell? {
        let indexPath = collectionView.indexPathForItemAtPoint(touchLocation)
        if let indexPath = indexPath {
            return collectionView.cellForItemAtIndexPath(indexPath) as? ImageGridCollectionViewCell
        }
        return nil
    }

    /**
    Update viewModel based on location where touches ended.
    */
    func gestureEndedAtTouchLocation(touchLocation: CGPoint?, completion: (animationAction: AnimationAction) -> ()) {
        if let touchLocation = touchLocation {

            let fromIndexPath = collectionView.indexPathForCell(gestureManager.currentCell!)
            let toIndexPath = collectionView.indexPathForItemAtPoint(touchLocation)
            let identitiy = fromIndexPath == toIndexPath // source and destination indexPaths are the same
            var indexPaths: [NSIndexPath] = []

            if let fromIndexPath = fromIndexPath where
                !identitiy {

                indexPaths.append(fromIndexPath)

                // Cell has been dragged onto trash bin
                if CGRectContainsPoint(gridView.trashBinFrame, touchLocation) {
                    viewModel?.deleteImageAtIndexPath(fromIndexPath, completion: { () -> () in
                        completion(animationAction: AnimationAction.Delete)
                        self.reloadIndexPaths(indexPaths)
                        return
                    })
                }

                if let toIndexPath = toIndexPath where
                    viewModel?.collectionList[toIndexPath.item] != nil &&
                    !identitiy {

                    indexPaths.append(toIndexPath)

                    viewModel?.swapImagesAtIndexPaths(fromIndexPath, toIndexPath: toIndexPath)
                    let toCell = collectionView.cellForItemAtIndexPath(toIndexPath) as? ImageGridCollectionViewCell
                    gestureManager.currentCell?.userInteractionEnabled = false
                    completion(animationAction: AnimationAction.Swap(toCell!))

                    self.reloadIndexPaths(indexPaths)
                }
                else {
                    completion(animationAction: AnimationAction.Reset)
                }
            }
            else {
                completion(animationAction: AnimationAction.Reset)
            }
        }
        else {
            completion(animationAction: AnimationAction.Reset)
        }
    }

    /**
    Reload specified indexPaths without disrupting snap animation.
    */
    func reloadIndexPaths(indexPaths: [NSIndexPath]) -> Void {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(0.25 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                UIView.performWithoutAnimation({ () -> Void in
                    self.collectionView.performBatchUpdates({ () -> Void in
                        self.collectionView.reloadItemsAtIndexPaths(indexPaths)
                        }, completion:nil)
                })
            })
        }
    }

}
