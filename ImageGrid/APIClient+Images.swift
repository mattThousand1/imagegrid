//
//  APIClient+Images.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/7/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import Foundation

extension APIClient {

    /**
    Fetch a single image given an id

    - parameter id:     id of the image
    - parameter completion:    A closure that takes an `APIResult` object consisting of either a JSON array or an `ErrorType` object.
    */
    func fetchSingleImage(id: String, completion: JSONObjectCompletion) -> NSURLSessionDataTask {

        return request(Images(), identifier: id, headers: nil) { [weak self] (data, urlResponse, error) -> () in
            guard let error = error else {
                let jsonOptional = try? NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                if let JSON = jsonOptional {
                    self?.validateJSONResponseIsObject(JSON, completion: completion)
                }
                return
            }
            completion(JSONObjectResult.failure(error))
        }

    }

    /**
    Fetch an array of imageIds

    - parameter completion:    A closure that takes an `APIResult` object consisting of either a JSON array or an `ErrorType` object.
    */
    func fetchImagesIds(completion: JSONObjectCompletion) -> NSURLSessionDataTask {

        return request(Images(), headers: nil) { [weak self] (data, urlResponse, error) -> () in
            guard let error = error else {
                let jsonOptional = try? NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                if let JSON = jsonOptional {
                    self?.validateJSONResponseIsObject(JSON, completion: completion)
                }
                return
            }
            completion(JSONObjectResult.failure(error))
        }

    }

    struct Images: APIEndpoint {

        var baseURLString: String {
            return "https://t23-pics.herokuapp.com"
        }

        var path: String {
            return "pics"
        }

    }

    /**
    Validate that the deserialized JSON represents an object.

    :param: JSONResponse The deserialized JSON.
    :param: completion   A completion closure that accepts a `JSONObjectResult`.
    */
    func validateJSONResponseIsObject(JSONResponse: AnyObject, completion: JSONObjectCompletion) {
        if let JSON = JSONResponse as? JSONObject {
            completion(JSONObjectResult.success(JSON))
        }
        else {
            let error = NSError(domain: "com.error.api", code: 0, userInfo: [NSLocalizedDescriptionKey: "Expected a JSON object, received \(JSONResponse)"])
            completion(JSONObjectResult.failure(error))
        }
    }
}
