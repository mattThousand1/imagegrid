//
//  APIClient.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/7/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import Foundation

/// A JSON object is an unordered set of name/value pairs.
typealias JSONObject = [String: AnyObject];

/// Convenience type representing an API result that is a JSON object.
typealias JSONObjectResult = APIResult<JSONObject>

/// Method signature for an API completion closure that consumes a JSON object.
typealias JSONObjectCompletion = (JSONObjectResult) -> Void

/**
An `APIResult` is the eventual return value of any API request. It represents
either a success value of type `T`, or a failure value of type `NSError`.

- Success: A value of type `T`.
- Failure: An `NSError` object describing the failure.
*/
enum APIResult<T> {
    case Success(T)
    case Failure(ErrorType)

    /**
    Create an `APIResult` indicating success.

    :param: value The API request's return value.

    :returns: A successful `APIResult` value.
    */
    static func success(value: T) -> APIResult<T> {
        return .Success(value)
    }

    /**
    Create an `APIResult` indicating failure.

    :param: error The `NSError` object describing the failure.

    :returns: An `APIResult` failure state.
    */
    static func failure(error: ErrorType) -> APIResult<T> {
        return .Failure(error)
    }

}

/**
The `APIEndpoint` protocol encapsulates the high-level details necessary to
communicate with an API endpoint.
*/
protocol APIEndpoint {

    /// The base per-endpoint.
    var baseURLString: String { get }

    /// The endpoint's path, relative to the base URL.
    var path: String { get }

}

final class APIClient {

    /**
    Perform an API request against an endpoint.

    :param: endpoint   API endpoint details.
    :param: parameters Optional API parameters.

    */
    func request(endpoint: APIEndpoint, identifier: String? = nil, headers:[String : String]? = nil, completion:(data: NSData?, urlResponse: NSURLResponse?, error: NSError?) -> ()) -> NSURLSessionDataTask {
        let id = identifier ?? ""
        let URL = NSURL(string: endpoint.baseURLString + "/" + endpoint.path + "/" + id)!
        let req = NSURLRequest(URL: URL)
        let task = NSURLSession.sharedSession().dataTaskWithRequest(req) { (data, urlResponse, error) -> Void in
            completion(data: data, urlResponse: urlResponse, error: error)
        }
        task.resume()
        return task
    }
}
