//
//  BaseService.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/7/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import Foundation

/// A closure that takes an optional `NSError` argument and has no return value.
typealias ErrorCompletion = (error: ErrorType?) -> Void

/**
The `BaseService` implements basic request management for an API
client. Each major endpoint should have a concrete subclass that implements
the necessary API calls.
*/
class BaseService {
    let client: APIClient
    private let pendingRequests = NSHashTable.weakObjectsHashTable()

    init(client: APIClient) {
        self.client = client
    }

    deinit {
        self.cancelAllRequests()
    }

    /**
    Cancel all pending requests.
    */
    func cancelAllRequests() {
        let requests = pendingRequests.allObjects ?? []
        for request in requests {
            request.cancel()
        }
    }

}

// MARK: - Internal

extension BaseService {

    func notePendingRequest(request: NSURLSessionDataTask) {
        pendingRequests.addObject(request)
    }

}