//
//  Image.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/8/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import UIKit

class Image: NSObject {

    /**
    Url of image resource on S3.
    */
    var imageUrl: String?

    convenience init(imageUrl: String) {
        self.init()
        self.imageUrl = imageUrl
    }
}
