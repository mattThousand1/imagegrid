//
//  ImageGridCollectionViewCell.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/8/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import UIKit

final class ImageGridCollectionViewCell: UICollectionViewCell {

    let imageView: UIImageView = UIImageView(frame: CGRectZero)

    /**
    URL of image resource on S3.
    */
    var imageUrl: String? {
        didSet {
            if let imageUrl = imageUrl {
                let url = NSURL(string: imageUrl) ?? NSURL()
                let data = NSData(contentsOfURL: url) ?? NSData()

                imageView.image = UIImage(data: data)
            }
        }
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        reset()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        hidden = false
    }

    /**
    Animate "pop" transform for cell
    */
    func pop() -> Void {
        self.transform = CGAffineTransformMakeScale(1.2, 1.2)
        UIView.animateWithDuration(0.6) { () -> Void in
            self.transform = CGAffineTransformIdentity
        }
    }
}

private extension ImageGridCollectionViewCell {

    /**
    Set up constraints for imageView.
    */
    func reset() -> Void {
        // Subviews
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)

        // Layout
        let topConstraint = NSLayoutConstraint(item: contentView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: imageView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0.0)
        let leadingConstraint = NSLayoutConstraint(item: contentView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: imageView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0.0)
        let trailingConstraint = NSLayoutConstraint(item: contentView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: imageView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0.0)
        let bottomConstraint = NSLayoutConstraint(item: contentView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: imageView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0.0)

        contentView.addConstraints([
                topConstraint,
                leadingConstraint,
                trailingConstraint,
                bottomConstraint
            ])
    }

}
