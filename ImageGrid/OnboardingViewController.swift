//
//  OnboardingViewController.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/9/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import UIKit

/**
Enum to inform display strings for labels/buttons
*/
enum DisplayMode {
    case Onboarding
    case Refresh
}

protocol OnboardingViewControllerDelegate {
    func fetchImagesButtonTapped() -> Void
}

class OnboardingViewController: UIViewController {

    let fetchImagesButton: UIButton = UIButton(frame: CGRectZero)
    let displayMessageLabel: UILabel = UILabel(frame: CGRectZero)
    var delegate: OnboardingViewControllerDelegate?
    var displayMode: DisplayMode = .Onboarding
    private var tertiaryButtonText: String = "Here we go!"

    private var displayMessage: String {
        return displayMode == .Onboarding ? "Hi Thirteen23" : "No more images..."
    }

    private var primaryButtonText: String {
        return displayMode == .Onboarding ? "Get Started" : "Tap to refresh"
    }

    private var secondaryButtonText: String {
        return displayMode == .Onboarding ? "OK" : "Awesome"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
        visualEffectView.frame = view.bounds
        view.addSubview(visualEffectView)


        // Display label
        displayMessageLabel.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.addSubview(displayMessageLabel)

        displayMessageLabel.textColor = .whiteColor()
        displayMessageLabel.font = UIFont(name: "AvenirNext-Regular", size: 26.0)!
        

        // Fetch images button
        fetchImagesButton.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.addSubview(fetchImagesButton)

        fetchImagesButton.addTarget(self, action: Selector("fetchImagesButtonTapped"), forControlEvents: UIControlEvents.TouchUpInside)
        styleFetchImagesButton()

        configureLayout()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        fetchImagesButton.setTitle(NSLocalizedString(primaryButtonText, comment: "pre-tap button text"), forState: UIControlState.Normal)
        displayMessageLabel.text = NSLocalizedString(displayMessage, comment: "display message to greet user")
    }

    /**
    Animate button transform, notify delegate
    */
    func fetchImagesButtonTapped() -> Void {

        let duration = 2.0
        let delay = 0.5
        let options = UIViewKeyframeAnimationOptions.CalculationModeCubic

        fetchImagesButton.setTitle(NSLocalizedString(secondaryButtonText, comment: "initial animation button text"), forState: UIControlState.Normal)

        UIView.animateKeyframesWithDuration(duration, delay: delay, options: options, animations: {
            UIView.addKeyframeWithRelativeStartTime(0, relativeDuration: 13/4, animations: {
                UIView.animateWithDuration(1.5, delay: 0.0, usingSpringWithDamping: 0.65, initialSpringVelocity: 1.0, options: .CurveEaseIn, animations: { () -> Void in
                    self.fetchImagesButton.transform = CGAffineTransformMakeScale(3.5, 3.5)
                }, completion: nil)
            })
            UIView.addKeyframeWithRelativeStartTime(3/4, relativeDuration: 1/4, animations: {
                self.fetchImagesButton.setTitle(NSLocalizedString(self.tertiaryButtonText, comment: "final button text"), forState: UIControlState.Normal)
                UIView.animateWithDuration(1.0, delay: 0.0, usingSpringWithDamping: 0.65, initialSpringVelocity: 1.0, options: .CurveEaseOut, animations: { () -> Void in
                    self.fetchImagesButton.transform = CGAffineTransformIdentity
                }, completion: nil)
            })

            }, completion: nil)

        delegate?.fetchImagesButtonTapped()
    }

}

private extension OnboardingViewController {

    /**
    Add layout constraints to subviews
    */
    func configureLayout() -> Void {
        let labelCenterXConstraint = NSLayoutConstraint(item: displayMessageLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0.0)
        let labelCenterYConstraint = NSLayoutConstraint(item: displayMessageLabel, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: -100.0)

        let buttonWidthConstraint = NSLayoutConstraint(item: fetchImagesButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 200.0)
        let buttonHeightConstraint = NSLayoutConstraint(item: fetchImagesButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 50.0)
        let buttonCenterXConstraint = NSLayoutConstraint(item: fetchImagesButton, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0.0)
        let buttonCenterYConstraint = NSLayoutConstraint(item: fetchImagesButton, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: 0.0)

        view.addConstraints([
            buttonWidthConstraint,
            buttonHeightConstraint,
            buttonCenterXConstraint,
            buttonCenterYConstraint,
            labelCenterXConstraint,
            labelCenterYConstraint
            ])
    }
    
    /**
    Specify colors, fonts for fetchImagesButton
    */
    func styleFetchImagesButton() -> Void {
        fetchImagesButton.backgroundColor = snazzyBluishColor
        fetchImagesButton.titleLabel?.font = UIFont(name: "AvenirNext-Regular", size: 18.0)!
        fetchImagesButton.layer.cornerRadius = 25.0
    }

}
