//
//  ImageGridViewModel.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/8/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol ImageGridViewModelDelegate {

    func imageFetchDidFail(errorMessage: String) -> Void

    func imageGridViewModelDidFetchImages() -> Void

    func imageGridViewModelImagesCacheDidEmpty() -> Void

}

final class ImageGridViewModel: NSObject {

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let imagesService = ImagesService(client: APIClient())
    var collectionList: [Image?] = []
    var delegate: ImageGridViewModelDelegate?

    /**
    Fetch images from imagesService.
    */
    func fetchImages(completion: () -> ()) -> Void {
        reset()
        imagesService.fetchImages { (error) -> Void in
            guard let _ = error else {
                self.collectionList = self.imagesService.images
                self.delegate?.imageGridViewModelDidFetchImages()

                // Persist images
                self.save()
                self.load()
                completion()
                return
            }

            self.delegate?.imageFetchDidFail("Something went wrong fetching images...")
            completion()
        }
    }

    /**
    Persist images in core data
    */
    func save() -> Void {
        for image in self.collectionList {
            let managedContext = appDelegate.managedObjectContext
            let entity =  NSEntityDescription.entityForName("Image", inManagedObjectContext:managedContext)
            let newImage = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            if let image = image {
                //persist image and image url
                newImage.setValue(image.imageUrl, forKey: "imageUrl")
            }
            else {
                // persist image without image url
                newImage.setValue(nil, forKey: "imageUrl")
            }
            do {
                try managedContext.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }

        }
    }

    /**
    Load images from core data
    */
    func load() -> Void {
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Image")

        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            for result in results {
                images.append(result as? NSManagedObject)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    /**
    Update collectionList and images cache for swapped images
    */
    func swapImagesAtIndexPaths(fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) -> Void {
        let fromManagedObject = images[fromIndexPath.item]
        let toManagedObject = images[toIndexPath.item]
        let fromImage = collectionList[fromIndexPath.item]
        let toImage = collectionList[toIndexPath.item]

        images[toIndexPath.item] = fromManagedObject
        images[fromIndexPath.item] = toManagedObject
        collectionList[toIndexPath.item] = fromImage
        collectionList[fromIndexPath.item] = toImage
    }

    /**
    Delete image from core data, update images cache, update collectionList
    */
    func deleteImageAtIndexPath(indexPath: NSIndexPath?, completion: () -> ()) -> Void {

        if let indexPath = indexPath {
            // delete image from core data
            let managedContext = appDelegate.managedObjectContext
            if let imageToDelete = images[indexPath.item] {
                managedContext.deleteObject(imageToDelete)
            }
            images.removeAtIndex(indexPath.item)
            images.insert(nil, atIndex: indexPath.item)

            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }

            // reload collection list
            loadCollectionList()

            completion()
        }
    }

    var images = [NSManagedObject?]() {
        didSet {
            loadCollectionList()
        }
    }

    /**
    Setter notifies delegate that all images have been
    deleted from database
    */
    var allImagesDeleted: Bool = false {
        didSet {
            if allImagesDeleted {
                delegate?.imageGridViewModelImagesCacheDidEmpty()
            }
        }
    }
}

private extension ImageGridViewModel {

    /**
    Update collectionList from core data
    */
    func loadCollectionList() -> Void {
        collectionList = []
        var collectionListHasRemainingImages: Bool = false
        for image in images {
            if let image = image {
                let imageUrl: String = image.valueForKey("imageUrl") as? String ?? ""
                self.collectionList.append(Image(imageUrl: imageUrl))
                collectionListHasRemainingImages = true
            }
            else {
                self.collectionList.append(nil)
            }
        }
        allImagesDeleted = !collectionListHasRemainingImages && collectionList.count > 0
    }

    /**
    Re-instantiate collectionList and images cache.
    */
    func reset() -> Void {
        collectionList = []
        images = []
        clean()
    }

    /**
    Clear images from database
    */
    func clean() -> Void {
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Image")

        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            for result in results {
                if let result = result as? NSManagedObject {
                    managedContext.deleteObject(result)
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

}
