//
//  GestureManager.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/10/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import UIKit

/**
Enum for translating gesture on
collectionView cell into snap
animation
*/
enum AnimationAction {
    case Reset
    case Swap(ImageGridCollectionViewCell)
    case Delete
}

protocol GestureManagerDelegate {

    func cellForTouchLocation(touchLocation: CGPoint) -> ImageGridCollectionViewCell?
    func gestureEndedAtTouchLocation(touchLocation: CGPoint?, completion: (animationAction: AnimationAction) -> ()) -> Void

}

class GestureManager: NSObject {

    var animator: UIDynamicAnimator?
    var snap: UISnapBehavior?
    var delegate: GestureManagerDelegate?
    var currentCell: ImageGridCollectionViewCell?
    var gestureRecognizer: UIPanGestureRecognizer!
    var animationInProgress: Bool = false
    private var initialCellCenter: CGPoint = CGPointZero

    override init() {
        super.init()
        gestureRecognizer = UIPanGestureRecognizer(target: self, action: Selector("handleGesture:"))
        gestureRecognizer?.delegate = self
    }

    /**
    Handle gesture on collectionViewCell, conditionally add
    snap animation
    */
    func handleGesture(guestureRecognizer: UIPanGestureRecognizer) -> Void {
        let touchPoint = gestureRecognizer.locationInView(view)

        switch (gestureRecognizer.state) {
        case .Began:
            // Set current cell
            animator?.removeAllBehaviors()
            currentCell = delegate?.cellForTouchLocation(touchPoint)
            initialCellCenter = currentCell?.center ?? CGPointZero
            break
        case .Failed:
            break
        case .Changed:
            // Reposition current cell
            currentCell?.center = touchPoint
            break
        case .Ended:
            // Nil-out current cell
            delegate?.gestureEndedAtTouchLocation(currentCell?.center, completion: { animationAction  in

                switch (animationAction) {
                    case .Reset:
                        if let currentCell = self.currentCell {
                            let swapCellSnap: UISnapBehavior = UISnapBehavior(item: currentCell, snapToPoint: self.initialCellCenter)
                            self.animator?.addBehavior(swapCellSnap)
                        }
                        self.currentCell = nil
                        break
                    case .Swap(let cell):
                        let swapCellSnap: UISnapBehavior = UISnapBehavior(item: cell, snapToPoint: self.initialCellCenter)
                        self.animator?.addBehavior(swapCellSnap)
                        if let currentCell = self.currentCell {
                            let location = cell.center
                            let snapToInitialFrame: UISnapBehavior = UISnapBehavior(item: currentCell, snapToPoint: location)
                            self.animator?.addBehavior(snapToInitialFrame)
                            currentCell.pop()
                        }
                        self.currentCell = nil
                        break
                    case .Delete:
                        self.currentCell?.hidden = true
                        self.currentCell = nil
                        break
                }
            })
            break
        default:
            break
        }
    }

    var view: UIView? {
        didSet {
            if let view = view {
                animator = UIDynamicAnimator(referenceView: view)
            }
        }
    }
}

extension GestureManager: UIGestureRecognizerDelegate {

    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        return true
    }

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

}
