//
//  ImagecollectionView.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/8/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import UIKit

final class ImageGridView: UIView {

    var trashBinFrame: CGRect = CGRectZero
    private var lidView: UIImageView?
    private var bodyView: UIImageView?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if let lidView = lidView,
            bodyView = bodyView {
            trashBinFrame = CGRectMake(
                CGRectGetMinX(lidView.frame) - 30,
                CGRectGetMinY(lidView.frame) - 30,
                CGRectGetWidth(lidView.frame) + 60,
                CGRectGetHeight(lidView.frame) + CGRectGetHeight(bodyView.frame) + 50
            )
        }
    }
    
    var collectionView: UICollectionView? {
        didSet {
            if let collectionView = collectionView {
                collectionView.translatesAutoresizingMaskIntoConstraints = false
                addSubview(collectionView)
                collectionView.backgroundColor = .clearColor()
                let topConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: collectionView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: 0.0)
                let leadingConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: collectionView, attribute: NSLayoutAttribute.Leading, multiplier: 1.0, constant: 0.0)
                let trailingConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: collectionView, attribute: NSLayoutAttribute.Trailing, multiplier: 1.0, constant: 0.0)
                let bottomConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: collectionView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0.0)

                self.addConstraints([
                        topConstraint,
                        leadingConstraint,
                        trailingConstraint,
                        bottomConstraint
                    ])
                }
            }
    }

}

private extension ImageGridView {

    func commonInit() -> Void {
        let lid = UIImage(named: "trash_lid")
        let body = UIImage(named: "trash_body")
        lidView = UIImageView(image: lid)
        bodyView = UIImageView(image: body)

        if let lidView = lidView,
            bodyView = bodyView {
            lidView.translatesAutoresizingMaskIntoConstraints = false
            bodyView.translatesAutoresizingMaskIntoConstraints = false

            addSubview(lidView)
            addSubview(bodyView)

            let bodyViewWidthConstraint = NSLayoutConstraint(item: bodyView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 70.0)
            let bodyViewHeightConstraint = NSLayoutConstraint(item: bodyView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 70.0)
            let bodyViewCenterXConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: bodyView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0.0)
            let bodyViewBottomConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: bodyView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 40.0)
            let lidViewBottomConstraint = NSLayoutConstraint(item: lidView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: bodyView, attribute: NSLayoutAttribute.Top, multiplier: 1.0, constant: -10.0)
            let lidViewCenterXConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: lidView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0.0)
            let lidViewWidthConstraint = NSLayoutConstraint(item: lidView, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 80.0)
            let lidViewHeightConstraint = NSLayoutConstraint(item: lidView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 24.0)

            self.addConstraints([
                bodyViewWidthConstraint,
                bodyViewHeightConstraint,
                    bodyViewCenterXConstraint,
                    bodyViewBottomConstraint,
                    lidViewBottomConstraint,
                    lidViewCenterXConstraint,
                    lidViewWidthConstraint,
                    lidViewHeightConstraint
                ])
        }
    }


}
