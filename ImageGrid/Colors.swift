//
//  Colors.swift
//  ImageGrid
//
//  Created by Matthew Buckley on 11/11/15.
//  Copyright © 2015 Matt Buckley. All rights reserved.
//

import Foundation
import UIKit

public let snazzyBluishColor: UIColor = UIColor(red: 120.0 / 255.0, green: 154.0 / 255.0, blue: 159.0 / 255.0, alpha: 1.0) 
