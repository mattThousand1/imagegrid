# Matt Buckley #

A simple app built based on Thirteen23's iOS exercise specifications (https://github.com/thirteen23/ios-exercise)

## Onboarding  
------
The user is presented with a friendly greeting inviting them to fetch images on launch:  

![Simulator Screen Shot Nov 12, 2015, 2.25.36 PM.png](https://bitbucket.org/repo/AdreKj/images/1522418191-Simulator%20Screen%20Shot%20Nov%2012,%202015,%202.25.36%20PM.png)


## Refreshing  
------

Once all images have been dragged into the trash bin, the user is prompted to fetch a new set of images:  

![Simulator Screen Shot Nov 12, 2015, 2.24.13 PM.png](https://bitbucket.org/repo/AdreKj/images/3616426359-Simulator%20Screen%20Shot%20Nov%2012,%202015,%202.24.13%20PM.png)

## Error Messaging  
------  
Network errors are bubbled up to the UI and presented as an alert dialog:  

![Simulator Screen Shot Nov 12, 2015, 2.24.19 PM.png](https://bitbucket.org/repo/AdreKj/images/2410300079-Simulator%20Screen%20Shot%20Nov%2012,%202015,%202.24.19%20PM.png)

## Notes 
------  
- Build target: iOS 9
- Supported device orientations: portrait
- Supported devices: iPhone 6s Plus, iPhone 6 Plus, iPhone 6s, iPhone 6, iPhone 5s, iPhone 5
- Images are persisted between app launches
- Image order is *not* persisted between app launches